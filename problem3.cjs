function three(inventory) 
{
    let car_models_list = [];
    for (let v = 0; v < inventory.length; v++) 
    {
        car_models_list.push(inventory[v].car_model);
    }
    car_models_list.sort();
    return car_models_list;
}
module.exports = three;
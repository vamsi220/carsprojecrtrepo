function one(inventory) 
{
    for (let v = 0; v < inventory.length; v++) 
    {
        if (inventory[v].id ==33) 
        {
            return `Car 33 is a ${inventory[v].car_year} ${inventory[v].car_make} ${inventory[v].car_model}`;
        }
    }
}
module.exports = one;
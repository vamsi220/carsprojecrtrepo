var four = require('./problem4.cjs');

function five(inventory) 
{
    var car_age = four(inventory);
    let older_cars = [];
    for (let v = 0; v < car_age.length; v++) 
    {
        if (car_age[v] < 2000) 
        {
            older_cars.push(car_age[v]);
        }
    }
    return older_cars;
}
module.exports = five;